﻿using AnswerDigitalDotNet.Utilities;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerDigitalDotNet.PageObjects
{
    class KeypressPage
    {
        public static  IWebElement TextField(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.Id("target")));
        }

        public static IWebElement wholePage(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.TagName("html")));
        }

        public static string DisplayedKey(Browser browser)
        {

            string result = browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.Id("result"))).Text;
            result = result.Split(": ")[1];
            //alpha-numeric keys
            if(result.Length == 1)
            {
                return result;
            }
            else if (result.Contains("NUMPAD"))
            {
                //NUMPAD number keys
                switch (result)
                {
                    case "NUMPAD0":
                        return Keys.NumberPad0;
                    case "NUMPAD1":
                        return Keys.NumberPad1;
                    case "NUMPAD2":
                        return Keys.NumberPad2;
                    case "NUMPAD3":
                        return Keys.NumberPad3;
                    case "NUMPAD4":
                        return Keys.NumberPad4;
                    case "NUMPAD5":
                        return Keys.NumberPad5;
                    case "NUMPAD6":
                        return Keys.NumberPad6;
                    case "NUMPAD7":
                        return Keys.NumberPad7;
                    case "NUMPAD8":
                        return Keys.NumberPad8;
                    case "NUMPAD9":
                        return Keys.NumberPad9;
                    default:
                        Assert.Fail("Unsupported Key");
                        break;


                }
            }
            else if(result.Length >= 2 && result.Length < 4 && result[0].ToString().Equals("F"))
            {
                //function keys(F1-F12)
                switch (Int16.Parse(result.Remove(0, 1)))
                {
                    case 1:
                        return Keys.F1;
                    case 2:
                        return Keys.F2;
                    case 3:
                        return Keys.F3;
                    case 4:
                        return Keys.F4;
                    case 5:
                        return Keys.F5;
                    case 6:
                        return Keys.F6;
                    case 7:
                        return Keys.F7;
                    case 8:
                        return Keys.F8;
                    case 9:
                        return Keys.F9;
                    case 10:
                        return Keys.F10;
                    case 11:
                        return Keys.F11;
                    case 12:
                        return Keys.F12;
                    default:
                        Assert.Fail("Unsupported Key");
                        break;

                }
            }
            else{
                switch (result)
                {
                    case "ESCAPE":
                        return Keys.Escape;
                    case "TAB":
                        return Keys.Tab;
                    case "SHIFT":
                        return Keys.Shift;
                    case "CONTROL":
                        return Keys.Control;
                    case "ALT":
                        return Keys.Alt;
                    case "SPACE":
                        return Keys.Space;
                    //arrow keys
                    case "UP":
                        return Keys.Up;
                    case "DOWN":
                        return Keys.Down;
                    case "LEFT":
                        return Keys.Left;
                    case "RIGHT":
                        return Keys.Right;
                    //utility keys
                    case "INSERT":
                        return Keys.Insert;
                    case "HOME":
                        return Keys.Home;
                    case "PAGE_UP":
                        return Keys.PageUp;
                    case "DELETE":
                        return Keys.Delete;
                    case "END":
                        return Keys.End;
                    case "PAGE_DOWN":
                        return Keys.PageDown;
                        //Numpad mathmetical operators
                    case "ADD":
                        return Keys.Add;
                    case "SUBTRACT":
                        return Keys.Subtract;
                    case "MULTIPLY":
                        return Keys.Multiply;
                    case "DIVIDE":
                        return Keys.Divide;
                    case "ENTER":
                        return Keys.Enter;
                    case "BACK_SLASH":
                        return @"\";
                    case "SLASH":
                        return "/";
                    case "PERIOD":
                        return ".";
                    case "COMMA":
                        return ",";
                    case "OPEN_BRACKET":
                        return "[";
                    case "CLOSE_BRACKET":
                        return "]";
                    case "PAUSE":
                        return Keys.Pause;
                    default:
                        Assert.Fail("Unsupported Key");
                        break;
                    
                }

            }
            return "";
        }
    }
}
